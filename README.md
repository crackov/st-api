# SmartTools API Server

Lead tracking software using GraphQL via Apollo, based on MongoDB using Mongoose.

Server is created to primarily run as a monolith, but it can be extended to run as multiple micro-services without changing the core services. Every part of the code can be switched without a hassle, as long as the interfaces stay the same.

This is a server for [SmartTools Web Application](https://gitlab.com/car492/st-web)

## Usage

To start in **devevlopment** mode run 
```
npm start
```

To help with development a seeder can be used by executing
```
npm run seed
```

Pre-commit hook is added to check lint errors. Use linter as following
```
npm run lint
```
Pre-push hook is added to run tests. Execute tests as following
```
npm test
```

To run in production mode execute
```
npm run start:prod
```

## Author

Cedomir Rackov