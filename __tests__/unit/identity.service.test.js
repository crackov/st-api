const IdentityService = require('../../src/services/identity/identity.service');
const data = require('../utils/identity-data');

describe('IdentityService', () => {
  it('should construct', () => {
    const service = new IdentityService(data.secret, {});

    expect(service).toBeInstanceOf(IdentityService);
  });

  describe('Functionality', () => {
    let service;

    const mockDAO = {
      create: jest.fn(),
    };

    beforeEach(() => {
      service = new IdentityService(data.secret, mockDAO);
    });
    it('should make identity', async () => {
      await service.make('1', 'test', 'manager');

      expect(mockDAO.create.mock.calls.length).toBe(1);
    });
  });
});
