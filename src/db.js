const mongoose = require('mongoose');

async function connect({ mongoUrl }) {
  mongoose.set('useCreateIndex', true);
  mongoose.set('useFindAndModify', false);
  await mongoose.connect(mongoUrl, { useNewUrlParser: true });
}

module.exports = { connect };
