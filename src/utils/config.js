const fs = require('fs');

const readConfig = path => JSON.parse(fs.readFileSync(path));

module.exports = () => {
  const defualtPath = './config/config.json';
  const env = process.env.NODE_ENV || 'development';
  const evnPath = `./config/config.${env}.json`;

  if (fs.existsSync(evnPath)) {
    return readConfig(evnPath);
  }

  return readConfig(defualtPath);
};
