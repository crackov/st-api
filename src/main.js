const { ApolloServer, mergeSchemas } = require('apollo-server');

const db = require('./db');
const identityModule = require('./services/identity/identity.module');
const userModule = require('./services/user/user.module');
const authModule = require('./services/auth/auth.module');
const leadModule = require('./services/lead/lead.module');

const config = require('./utils/config')();

async function main() {
  await db.connect(config.db);

  const { identityService, ROLES } = await identityModule.register(config.identityModule);
  const { leadGQL } = await leadModule.register();
  const { userService, userGQL } = await userModule.register();
  const { authGQL } = await authModule.register({}, { identityService, userService });

  const schema = mergeSchemas({
    schemas: [authGQL, userGQL, leadGQL],
  });

  const server = new ApolloServer({
    schema,
    context: async ({ req }) => {
      const tokenWithBearer = req.headers.authorization || '';
      const token = tokenWithBearer.split(' ')[1];

      const ctx = {
        userId: null,
        userRole: null,
        ROLES,
        requireUser() {
          if (!ctx.userId) {
            throw new Error('User Required');
          }
        },
        requireRole(roles) {
          const isValidRole = roles.includes(ctx.role);
          if (!isValidRole) {
            throw new Error('Not Authorized');
          }
        },
      };

      if (token) {
        const decoded = await identityService.verify(token);
        Object.assign(ctx, decoded);
      }

      return ctx;
    },
    formatError: error => {
      // eslint-disable-next-line no-console
      console.log(error);
      return error;
    },
  });

  try {
    const { url } = await server.listen({ port: config.PORT });
    // eslint-disable-next-line no-console
    console.log(`Server ready at ${url}`);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.err(err);
  }
}

main();
