const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  username: { type: String, unique: true },
  identity: { type: Schema.Types.ObjectId, ref: 'Identity' },
});

const UserModel = model('User', userSchema);

module.exports = UserModel;
