class UserService {
  constructor(userDAO) {
    Object.defineProperties(this, {
      userDAO: { value: userDAO },
    });
  }

  async make(username) {
    const existingUser = await this.userDAO.findOne({ username });
    if (existingUser) {
      throw new Error('Username is not available');
    }
    const user = await this.userDAO.create({ username });

    return user;
  }

  async loadId({ username }) {
    const user = await this.userDAO.findOne({ username });
    if (!user) {
      return null;
    }

    return user.id;
  }

  async me({ ctx: { userId, role } }) {
    if (!userId) throw new Error('Auth required');

    const user = (await this.userDAO.findById(userId)).toObject();
    user.role = role;

    return Promise.resolve(user);
  }
}

module.exports = UserService;
