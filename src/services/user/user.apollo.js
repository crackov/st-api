const { makeExecutableSchema, gql } = require('apollo-server');

/**
 * Create Apollo GraphQL schema using single or multiple services.
 *
 * @param {Object} deps Service deps for resolvers
 */
const makeSchema = ({ userService }) => {
  return makeExecutableSchema({
    typeDefs: gql`
      type Me {
        _id: ID
        username: String
        role: String
      }

      type Query {
        me: Me
      }
    `,

    resolvers: {
      Query: {
        me: async (parent, args, ctx) => userService.me({ args, ctx }),
      },
    },
  });
};

module.exports = { makeSchema };
