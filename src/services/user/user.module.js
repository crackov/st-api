const UserModel = require('./user.model');
const UserService = require('./user.service');
const userApollo = require('./user.apollo');

async function register() {
  const userService = new UserService(UserModel);
  const userGQL = userApollo.makeSchema({ userService });

  return { userGQL, userService };
}

module.exports = { register };
