const { Schema, model } = require('mongoose');

// Mongoose has to register the Model, but it is never used directly
require('./category.model');

const leadSchema = new Schema(
  {
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    phone: String,
    email: String,
    address: String,
  },
  { discriminatorKey: 'kind' },
);

const LeadModel = model('Lead', leadSchema);

module.exports = LeadModel;
