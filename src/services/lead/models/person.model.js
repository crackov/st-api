const { Schema } = require('mongoose');

const LeadModel = require('./lead.model');
const { UNKNOWN, MALE, FEMALE } = require('../genders.const');

const PersonModel = LeadModel.discriminator(
  'Person',
  new Schema(
    {
      givenName: String,
      familyName: String,
      gender: { type: String, enum: [UNKNOWN, MALE, FEMALE], default: UNKNOWN },
    },
    { discriminatorKey: 'kind' },
  ),
);

module.exports = PersonModel;
