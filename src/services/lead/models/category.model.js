const { Schema, model } = require('mongoose');

const categorySchema = new Schema({
  name: { type: String, unique: true },
});

const CategoryModel = model('Category', categorySchema);

module.exports = CategoryModel;
