const { Schema } = require('mongoose');

const LeadModel = require('./lead.model');

const CompanyModel = LeadModel.discriminator(
  'Company',
  new Schema(
    {
      name: String,
      contactPersonName: String,
      website: String,
    },
    { discriminatorKey: 'kind' },
  ),
);

module.exports = CompanyModel;
