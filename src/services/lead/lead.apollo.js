const { makeExecutableSchema, gql } = require('apollo-server');

/**
 * Create Apollo GraphQL schema using single or multiple services.
 *
 * @param {Object} deps Service deps for resolvers
 */
const makeSchema = ({ leadService }) => {
  return makeExecutableSchema({
    typeDefs: gql`
      type Category {
        _id: ID
        name: String
      }

      interface Lead {
        _id: ID
        kind: String
        category: Category
        phone: String
        email: String
        address: String
      }

      enum Gender {
        unknown
        male
        female
      }

      type Person implements Lead {
        _id: ID
        kind: String
        category: Category
        phone: String
        email: String
        address: String
        givenName: String
        familyName: String
        gender: Gender
      }

      type Company implements Lead {
        _id: ID
        kind: String
        category: Category
        phone: String
        email: String
        address: String
        name: String
        contactPersonName: String
        website: String
      }

      input AddLeadInput {
        kind: String
        category: ID
        phone: String
        email: String
        address: String
        givenName: String
        familyName: String
        gender: Gender
        name: String
        contactPersonName: String
        website: String
      }

      input UpdateLeadInput {
        _id: ID!
        kind: String
        category: ID
        phone: String
        email: String
        address: String
        givenName: String
        familyName: String
        gender: Gender
        name: String
        contactPersonName: String
        website: String
      }

      input AddPersonInput {
        category: ID
        phone: String
        email: String
        address: String
        givenName: String
        familyName: String
        gender: Gender
      }

      input UpdatePersonInput {
        _id: ID!
        kind: String
        category: ID
        phone: String
        email: String
        address: String
        givenName: String
        familyName: String
        gender: Gender
      }

      input AddCompanyInput {
        category: ID
        phone: String
        email: String
        address: String
        name: String
        contactPersonName: String
        website: String
      }

      input UpdateCompanyInput {
        _id: ID!
        kind: String
        category: ID
        phone: String
        email: String
        address: String
        name: String
        contactPersonName: String
        website: String
      }

      type Query {
        leads: [Lead]
        lead(_id: ID): Lead
        categories: [Category]
      }

      type Mutation {
        addPerson(input: AddPersonInput!): Person!
        updatePerson(input: UpdatePersonInput!): Person
        addCompany(input: AddCompanyInput!): Company!
        updateCompany(input: UpdateCompanyInput!): Company
        addLead(input: AddLeadInput!): Lead!
        updateLead(input: UpdateLeadInput!): Lead
        removeLead(_id: ID): Lead
      }
    `,

    resolvers: {
      Query: {
        leads: async (parent, args, ctx) => leadService.listLeads(ctx),
        lead: async (parent, args, ctx) => leadService.loadLead({ args, ctx }),
        categories: async (parent, args, ctx) => leadService.listCategories({ ctx }),
      },
      Mutation: {
        addPerson: async (parent, { input }, ctx) => leadService.makePerson({ args: input, ctx }),
        updatePerson: async (parent, { input }, ctx) =>
          leadService.editPerson({ args: input, ctx }),
        addCompany: async (parent, { input }, ctx) => leadService.makeCompany({ args: input, ctx }),
        updateCompany: async (parent, { input }, ctx) =>
          leadService.editCompany({ args: input, ctx }),
        addLead: async (parent, { input }, ctx) => leadService.makeLead({ args: input, ctx }),
        updateLead: async (parent, { input }, ctx) => leadService.editLead({ args: input, ctx }),
        removeLead: async (parent, args, ctx) => leadService.killLead({ args, ctx }),
      },
      Lead: {
        __resolveType: obj => obj.kind,
      },
    },
  });
};

module.exports = { makeSchema };
