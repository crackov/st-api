const CategoryModel = require('./models/category.model');
const LeadModel = require('./models/lead.model');
const PersonModel = require('./models/person.model');
const CompanyModel = require('./models/company.model');
const LeadService = require('./lead.service');
const leadApollo = require('./lead.apollo');

async function register() {
  const leadService = new LeadService(LeadModel, PersonModel, CompanyModel, CategoryModel);
  const leadGQL = leadApollo.makeSchema({ leadService });

  return { leadGQL };
}

module.exports = { register };
