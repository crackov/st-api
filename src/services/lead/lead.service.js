/**
 * Utility function to get DAO name
 *
 * @param {string} kind Lead discriminator
 * @returns {string} DAO name
 */
function kindToDAOName(kind) {
  return `${kind.toLowerCase()}DAO`;
}

class LeadService {
  constructor(leadDAO, personDAO, companyDAO, categoryDAO) {
    Object.defineProperties(this, {
      leadDAO: { value: leadDAO },
      personDAO: { value: personDAO },
      companyDAO: { value: companyDAO },
      categoryDAO: { value: categoryDAO },
    });
  }

  async listCategories({ ctx: { requireUser } }) {
    requireUser();

    const categories = await this.categoryDAO.find();

    return categories.map(el => el.toObject());
  }

  async makePerson({ args, ctx }) {
    const leadArgs = Object.assign({ kind: 'Person' }, args);
    return this.makeLead({ args: leadArgs, ctx });
  }

  async editPerson({ args, ctx }) {
    return this.editLead({ args, ctx });
  }

  async makeCompany({ args, ctx }) {
    const leadArgs = Object.assign({ kind: 'Company' }, args);
    return this.makeLead({ args: leadArgs, ctx });
  }

  async editCompany({ args, ctx }) {
    return this.editLead({ args, ctx });
  }

  async makeLead({ args, ctx: { requireUser } }) {
    requireUser();

    const daoName = kindToDAOName(args.kind);
    const lead = await this[daoName].create(args);

    return lead.toObject();
  }

  async editLead({ args, ctx: { requireUser } }) {
    requireUser();

    const daoName = kindToDAOName(args.kind);
    const { _id } = args;
    const updateParams = Object.assign({}, args);
    delete updateParams._id;

    let lead = await this.leadDAO.findById(_id);
    if (lead.kind !== args.kind) {
      lead = await this.leadDAO.findByIdAndUpdate(_id, { kind: args.kind });
    }

    lead = await this[daoName].findByIdAndUpdate(_id, updateParams, { new: true, overwrite: true });
    console.log(lead, args);

    return lead && lead.toObject();
  }

  async loadLead({ args: { _id }, ctx: { requireUser } }) {
    requireUser();

    const lead = await this.leadDAO.findById(_id);

    // GraphQL should't return error when entity is not found
    // https://github.com/apollographql/apollo-link-rest/pull/142
    return lead && lead.toObject();
  }

  async listLeads({ requireUser }) {
    requireUser();

    const leads = await this.leadDAO.find().populate('category');

    return leads.map(el => el.toObject());
  }

  async killLead({ args: { _id }, ctx: { requireRole, ROLES } }) {
    requireRole([ROLES.MANAGER]);

    const lead = await this.leadDAO.findByIdAndRemove(_id);

    return lead && lead.toObject();
  }
}

module.exports = LeadService;
