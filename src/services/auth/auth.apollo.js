const { makeExecutableSchema, gql } = require('apollo-server');

/**
 * Create Apollo GraphQL schema using single or multiple services.
 *
 * @param {Object} deps Service deps for resolvers
 */
const makeSchema = ({ authService }) => {
  return makeExecutableSchema({
    typeDefs: gql`
      type LoginResponse {
        token: String
        role: String
      }

      type Mutation {
        login(username: String, password: String): LoginResponse
      }

      type Query {
        isAuthAvailable: Boolean
      }

      schema {
        mutation: Mutation
        query: Query
      }
    `,

    resolvers: {
      Query: {
        isAuthAvailable: () => true,
      },
      Mutation: {
        login: async (parent, { username, password }) => {
          const { token, role } = await authService.login(username, password);

          return { token, role };
        },
      },
    },
  });
};

module.exports = { makeSchema };
