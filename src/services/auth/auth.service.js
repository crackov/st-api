/**
 * Authentication and autorization service.
 *
 * @class
 */
class AuthService {
  /**
   * @param {IdentityService} identityService Identity verification for users
   * @param {UserService} userService Service for matching users
   */
  constructor(identityService, userService) {
    Object.defineProperties(this, {
      identityService: { value: identityService },
      userService: { value: userService },
    });
  }

  /**
   * Create a token for a valid user.
   *
   * @param {string} username Username of user to login
   * @param {string} password Password to verify against datastore
   * @returns {Promise<string>} Created token
   */
  async login(username, password) {
    const userId = await this.userService.loadId({ username });
    if (!userId) {
      throw new Error('User not found');
    }

    const { token, role } = await this.identityService.login(userId, password);

    return { token, role };
  }
}

module.exports = AuthService;
