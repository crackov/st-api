const AuthService = require('./auth.service');
const authApollo = require('./auth.apollo');

async function register(opts, { identityService, userService }) {
  const authService = new AuthService(identityService, userService);

  const authGQL = authApollo.makeSchema({ authService });

  return { authGQL };
}

module.exports = { register };
