const { Schema, model } = require('mongoose');
const { MANAGER, EMPLOYEE } = require('./roles.const');

const identitySchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User', unique: true },
  password: String,
  role: { type: String, enum: [MANAGER, EMPLOYEE] },
});

const IdentityModel = model('Identity', identitySchema);

module.exports = IdentityModel;
