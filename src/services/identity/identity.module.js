const ROLES = require('./roles.const');
const IdentityModel = require('./identity.model');
const IdentityService = require('./identity.service');

async function register(opts) {
  const identityService = new IdentityService(opts.secret, IdentityModel);

  return { identityService, ROLES };
}

module.exports = { register };
