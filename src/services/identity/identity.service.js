const jwt = require('jsonwebtoken');
const argon2 = require('argon2');

/**
 * Async wrapper for jsonwebtoken.sign method.
 *
 * @param {Object} payload Payload for JWT
 * @param {string} secret Sign JWT with this secret
 * @param {Object} options JWT options
 */
const signJWT = (payload, secret, options) =>
  new Promise((resolve, reject) => {
    jwt.sign(payload, secret, options, (err, token) => {
      if (err) return reject(err);

      return resolve(token);
    });
  });

/**
 * Async wrapper for jsonwebtoken.verify method.
 *
 * @param {string} token JWT to verify
 * @param {string} secret Secret to verify with
 * @param {Object} options JWT options
 */
const verifyJWT = (token, secret, options) =>
  new Promise((resolve, reject) => {
    jwt.verify(token, secret, options, (err, decoded) => {
      if (err) return reject(err);

      return resolve(decoded);
    });
  });

/**
 * Authentication and autorization service.
 *
 * @class
 */
class IdentityService {
  /**
   * @param {string} secret Configured secret for JWT
   * @param {Object} identityDAO DAO for matching users
   */
  constructor(secret, identityDAO) {
    Object.defineProperties(this, {
      secret: { value: secret },
      identityDAO: { value: identityDAO },
    });
  }

  /**
   * Create identity for an exiting user.
   * (Create user before adding identity)
   *
   * @param {string} userId User id
   * @param {string} password Plain password
   * @param {"manager" | "employee"} role Identity role
   */
  async make(userId, password, role) {
    const hash = await argon2.hash(password);
    const identity = await this.identityDAO.create({ userId, password: hash, role });

    return identity;
  }

  /**
   * Create a token for a valid user.
   *
   * @param {string} userId Id of user to login
   * @param {string} password Password to verify against datastore
   * @returns {Promise<string>} Created token
   */
  async login(userId, password) {
    const identity = await this.identityDAO.findOne({ userId });
    if (!identity) {
      throw new Error('Identity not found');
    }

    const isValidPassword = await argon2.verify(identity.password, password);
    if (!isValidPassword) {
      throw new Error('Password is not valid');
    }

    const token = await signJWT({ userId, role: identity.role }, this.secret);
    return { token, role: identity.role };
  }

  /**
   * Verify token
   *
   * @param {string} token Token provided by the client
   * @returns {Promise<Object>} Payload from the token
   */
  async verify(token) {
    let decoded = null;
    try {
      decoded = await verifyJWT(token, this.secret);
    } catch (err) {
      throw new Error('Token is not valid');
    }

    const identity = await this.identityDAO.findOne({ userId: decoded.userId });
    if (!identity) {
      throw new Error('Identity not found');
    }

    return decoded;
  }
}

module.exports = IdentityService;
