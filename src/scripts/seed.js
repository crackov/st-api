/* eslint-disable no-console */
const mongoose = require('mongoose');

const db = require('../db');
const IdentityModel = require('../services/identity/identity.model');
const IdentityService = require('../services/identity/identity.service');

const UserModel = require('../services/user/user.model');
const UserService = require('../services/user/user.service');

const CategoryModel = require('../services/lead/models/category.model');
const PersonModel = require('../services/lead/models/person.model');
const CompanyModel = require('../services/lead/models/company.model');

const { MANAGER, EMPLOYEE } = require('../services/identity/roles.const');
const { MALE } = require('../services/lead/genders.const');

const config = require('../utils/config')();

// TODO: Refactor
async function main() {
  // Connect to DB
  await db.connect(config.db);
  await mongoose.connection.db.dropDatabase();

  // Instaciate services
  const userService = new UserService(UserModel);
  const identityService = new IdentityService(config.identityModule.secret, IdentityModel);

  // START: Craete manager user
  const managerUser = await userService.make('admin');
  const managerIdentity = await identityService.make(managerUser._id, 'test', MANAGER);

  managerUser.identity = managerIdentity;
  console.log('MANAGER Saved');
  console.log('username: admin');
  console.log('password: test');
  console.log();

  const empUser = await userService.make('user');
  const empIdentity = await identityService.make(empUser._id, 'test', EMPLOYEE);

  empUser.identity = empIdentity;
  console.log('EMPLOYEE Saved');
  console.log('username: user');
  console.log('password: test');
  console.log();

  const categories = await CategoryModel.create([
    { name: 'new' },
    { name: 'won' },
    { name: 'lost' },
  ]);

  console.log('Categories Saved:', 'new, won, lost');
  console.log();

  const person = await PersonModel.create({
    phone: '+381000000',
    email: 'pera@example.com',
    address: 'Negde Daleko 78',
    givenName: 'Petar',
    familyName: 'Petrovic',
    gender: MALE,
    category: categories[0].id,
  });

  console.log('Person Saved');
  console.log('givenName:', person.givenName);
  console.log();

  const company = await CompanyModel.create({
    phone: '+381222222',
    email: 'acme@example.com',
    address: 'Negde Blize 33',
    name: 'Acme DOO',
    contactPersonName: 'Simeon',
    website: 'example.com',
    category: categories[1].id,
  });

  console.log('Company Saved');
  console.log('name:', company.name);
  console.log();

  console.log('Done');
  process.exit();
}

main()
  .catch(err => console.error(err))
  .then(() => process.exit());
